﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using FirebirdSql.Data.FirebirdClient;

namespace ConvertOrder
{
    class Program
    {
        static int Main(string[] args)
        {
//            string shablon =
//                @"Клиент       : [CodeClient]
//Получатель   : [CodeDostavki]
//Оплата       : 0
//ID заказа    : [ZakazCode]
//Клиентский ID: [ZakazCode]
//Дата заказа  : [DataOrder] 
//Позиций      : [Rows]
//Версия EXE   : ZCONVERT
//Версия CFG   : ZCONVERT
//Статус CFG   : ZCONVERT
//Прайс-лист   : [DataOrder] 
//Комментарий  : [Comment]
//";
            Cod cod;
            try
            {
                Encoding encoding = Encoding.GetEncoding(1251);
                #region Проверка параметров
                if (args.Length < 2)
                {
                    System.Console.WriteLine("Convert <файл заявки> <файл шаблона заявки>");
                    return 1;
                }

                if (!System.IO.File.Exists(args[0]) || !System.IO.File.Exists(args[1]))
                {
                    System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + " Convert <файл заявки> <файл шаблона заявки>");
                    return 1;
                }       
                #endregion
                System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + " Начинаем конвертацию. " + " " + args[0]);
                //Читаем файл заявки
                List<Row> rows = new List<Row>();
                Order order = new Order();
                XmlDocument doc = new XmlDocument();
                doc.Load(args[0]);
                IEnumerable<string> LinesCod = File.ReadAllLines(args[1], encoding);
                XmlNode orderxml = doc.SelectSingleNode("PACKET/ORDER");
                if (orderxml.SelectSingleNode("COMMENT")!=null)
                order.Comment = orderxml.SelectSingleNode("COMMENT").InnerText;
                cod = GetCodeFromASU(orderxml.SelectSingleNode("CLIENT_ID").InnerText);
                if (string.IsNullOrEmpty(cod.CodeDostavki)) 
                cod =  GetCodeFromASU(orderxml.SelectSingleNode("CLIENT_ID").InnerText + "_" + orderxml.SelectSingleNode("DEP_ID").InnerText);
                order.CodeDostavki = cod.CodeDostavki;
                order.CodeClient = cod.CodeClient;
                order.TimeOrder = "";
                order.DataOrder = orderxml.SelectSingleNode("ORDERDATE").InnerText;
                order.CodeZakaz = orderxml.SelectSingleNode("ORDER_ID").InnerText;
                int i = 0;
                if (orderxml.SelectSingleNode("ITEMS")==null) return 1;
                foreach (XmlNode item in doc.SelectNodes("//ITEM"))
                    {
                        rows.Add(new Row() { Code = item.SelectSingleNode("CODE").InnerText, Count = item.SelectSingleNode("QTTY").InnerText });     
                        i++;
                     }
                    order.rows = rows;
                    order.Rows = rows.Count.ToString();                  
                    //формируем заявку СИА
                    IEnumerable<string> shablon = File.ReadLines(args[1], encoding);
                    StringBuilder ordersia = new StringBuilder();
                    ordersia.Append(string.Join<string>("\r\n", shablon).Replace("[CodeZakaz]", order.CodeZakaz)
                                                                        .Replace("[CodeClient]", order.CodeClient)
                                                                        .Replace("[CodeDostavki]", order.CodeDostavki)
                                                                        .Replace("[AddressDostavki]", order.AddressDostavki)
                                                                        .Replace("[DataOrder]", order.DataOrder)
                                                                        .Replace("[Rows]", order.rows.Count.ToString())
                                                                        .Replace("[Code]", order.rows[0].Code)
                                                                        .Replace("[Count]", order.rows[0].Count)
                                                                        .Replace("[Comment]",order.Comment)).AppendLine();
                    foreach (var item in order.rows)
                    {
                        ordersia.Append(new string(' ', 50 - item.Code.Length)).Append(item.Code).Append(new string(' ', 10 - item.Count.Length)).Append(item.Count).AppendLine();
                    }
                    //пишем в файл 
                    File.WriteAllText(args[0] + ".txt", ordersia.ToString(), encoding);

            }
            catch (Exception e)
            {
                System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + e.Message);
                return 1;
            }

            return 0;
        }

        private static Cod GetCodeFromASU(string addresscode)
        {
            Cod cod = new Cod();
            System.Console.WriteLine("select nid,nidlib from adrdost a join nab n on a.adrdostid = n.nid where a.adrdostcode='{0}'", addresscode);

            using (FbConnection RefConnection = new FbConnection("User ID=TWUSER;Password=54321; Database=./db/Siafil.gdb; DataSource=asusrv; Charset=NONE; providerName=\"System.Data.SqlClient\""))
            {
                try
                {

                    RefConnection.Open();
                    FbCommand readCommand = new FbCommand(String.Format("select nid,nidlib from adrdost a join nab n on a.adrdostid = n.nid where a.adrdostcode='{0}'", addresscode), RefConnection);
                    FbDataReader myreader = readCommand.ExecuteReader();
                    int count = 1;
                    while (myreader.Read())
                    {
                        if (count > 1)
                        {
                            cod = null;
                            RefConnection.Close();
                            throw new Exception("В базе несколько одинаковых клиентских кодов " + addresscode);
                        }

                        cod.CodeDostavki = myreader.GetString(0);
                        cod.CodeClient = myreader.GetString(1);
                        count++;
                    }
                    RefConnection.Close();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
            return cod;
        }
        public class Order
        {
            public string CodeClient { get; set; }
            public string CodeDostavki { get; set; }
            public string AddressDostavki { get; set; }
            public string Rows { get; set; }
            public string DataOrder { get; set; }
            public string TimeOrder { get; set; }
            public string DataPrice { get; set; }
            public string TimePrice { get; set; }
            public string Comment { get; set; }
            public List<Row> rows { get; set; }
            public string CodeZakaz { get; set; }
        }
        public class Row
        {
            public string Code { get; set; }
            public string Count { get; set; }
            public string CodeDostavkiFarm { get; set; }
        }
        public class Cod
        {
            public string CodeDostavki { get; set; }
            public string CodeClient { get; set; }
        }

    }
}
